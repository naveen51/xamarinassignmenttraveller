﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinAssignment.Model;

namespace XamarinAssignment
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TravellerPage : ContentPage
    {
        public TravellerPage()
        {
            InitializeComponent();
        }

        private void SaveExperianceClicked_Clicked(object sender, EventArgs e)
        {

            Post post = new Post() {
                Experience = experianceEntry.Text
            };

            using(SQLiteConnection connection = new SQLiteConnection(App.DataBaseLocation)){
                connection.CreateTable<Post>();
                int rows = connection.Insert(post);
                //connection.Close(); This explicitly closing connection object is not requiered when you are using "using" statement.Coonection will get closed automatically when control leaving the block

                if (rows > 0)
                {
                    DisplayAlert("Success", "Experience Successfully Inserted !!!", "OK");
                }
                else
                {
                    DisplayAlert("Failure", "Experience Insertion Failed !!!", "OK");
                }

            }
        }
    }
}