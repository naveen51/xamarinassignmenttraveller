﻿using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinAssignment
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapPage : ContentPage
    {
        private bool hasLocationPermission = false;

        public MapPage()
        {
            
            InitializeComponent();
            GetPermissionAsync();
        }

        private async void GetPermissionAsync()
        {
            try {
                
                var status = await CrossPermissions.Current
                    .CheckPermissionStatusAsync(Permission.LocationWhenInUse);

                if (status != PermissionStatus.Granted)
                {

                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.LocationWhenInUse))
                    {

                        await DisplayAlert("Need your Location", "We need to access your location", "Ok");
                    }


                    var permissionStatus = await CrossPermissions.Current.RequestPermissionsAsync(Permission.LocationWhenInUse);

                    if (permissionStatus != null && permissionStatus.ContainsKey(Permission.LocationWhenInUse))
                    {

                        status = permissionStatus[Permission.LocationWhenInUse];
                    }


                    if (status == PermissionStatus.Granted)
                    {
                        hasLocationPermission = true;
                        locationMap.IsShowingUser = true;
                        GetLocationAsync();
                    }
                    else
                    {
                        await DisplayAlert("Location Denied", "You dint give location access to us,So we cant show the map", "Ok");
                    }
                }
                else {
                    locationMap.IsShowingUser = true;
                    GetLocationAsync();
                }


            }
            catch (Exception ex) {
                await DisplayAlert("Error", ex.Message, "Ok");
            }

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (hasLocationPermission) {
                var locator = CrossGeolocator.Current;
                locator.PositionChanged += Locator_PositionChanged; 
            }
            GetLocationAsync();
        }

        private void Locator_PositionChanged(object sender, PositionEventArgs e)
        {
            MoveToMap(e.Position);
        }

        private async Task GetLocationAsync()
        {
            if (hasLocationPermission) {
                var locator  = CrossGeolocator.Current;
                var position = await locator.GetPositionAsync();
                MoveToMap(position);
            }
        }

        private void MoveToMap(Position position)
        {
            locationMap.MoveToRegion(new Xamarin.Forms.Maps.MapSpan(
                    new Xamarin.Forms.Maps.Position(position.Latitude, position.Longitude), 1, 1));
        }
    }

}

   