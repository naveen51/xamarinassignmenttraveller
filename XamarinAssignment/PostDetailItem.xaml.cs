﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinAssignment.Model;

namespace XamarinAssignment
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PostDetailItem : ContentPage
	{
        private Post selectedPost;

		public PostDetailItem (Post selectedItem)
		{
            
			InitializeComponent ();
            this.selectedPost = selectedItem;
        }

        /**
         * 
         * 
         * **/
        private void UpdateButton_Clicked(object sender, EventArgs e)
        {
            if (selectedPost != null) {
                //update the existing entry
                selectedPost.Experience = experianceEntry.Text;

                using (SQLiteConnection connection = new SQLiteConnection(App.DataBaseLocation))
                {
                    connection.CreateTable<Post>();
                    int rows = connection.Update(selectedPost);
                    //connection.Close(); This explicitly closing connection object is not requiered when you are using "using" statement.Coonection will get closed automatically when control leaving the block

                    if (rows > 0)
                    {
                        DisplayAlert("Success", "Experience Successfully Update !!!", "OK");
                    }
                    else
                    {
                        DisplayAlert("Failure", "Experience Update Failed !!!", "OK");
                    }

                }
            }

        }

        private void DeletButton_Clicked(object sender, EventArgs e)
        {
            if (selectedPost != null)
            {
                //delete the existing entry

                using (SQLiteConnection connection = new SQLiteConnection(App.DataBaseLocation))
                {
                    connection.CreateTable<Post>();
                    int rows = connection.Delete(selectedPost);
                    //connection.Close(); This explicitly closing connection object is not requiered when you are using "using" statement.Coonection will get closed automatically when control leaving the block            

                    if (rows > 0)
                    {
                        DisplayAlert("Success", "Experience Successfully Dleted !!!", "OK");
                    }
                    else
                    {
                        DisplayAlert("Failure", "Experience Deletion Failed !!!", "OK");
                    }
                }

            }

        }
    }
}