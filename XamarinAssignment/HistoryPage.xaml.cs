﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinAssignment.Model;

namespace XamarinAssignment
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HistoryPage : ContentPage
    {
        public HistoryPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            using (SQLiteConnection connection = new SQLiteConnection(App.DataBaseLocation)) {
                connection.CreateTable<Post>();
                var posts = connection.Table<Post>().ToList();
                //connection.Close(); This explicitly closing connection object is not requiered when you are using "using" statement.Coonection will get closed automatically when control leaving the block

                //Binding data to Xaml i.e Binding the context
                postListView.ItemsSource = posts;
            }
        }

        private void PostListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var selectedPost = postListView.SelectedItem as Post;

            if (selectedPost != null) {
                //Navigate to Detail page
                Navigation.PushAsync(new PostDetailItem(selectedPost));
            }
            
        }
    }
}